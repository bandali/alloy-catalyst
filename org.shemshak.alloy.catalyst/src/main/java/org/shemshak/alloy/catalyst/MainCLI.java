/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2018-2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import picocli.CommandLine;
import picocli.CommandLine.Command;


@Command(
         name = "catalyst",
         version = {
             "catalyst 0.1",
             "Copyright (C) 2018-2019 Amin Bandali",
             "License: LGPLv3+ <https://gnu.org/licenses/lgpl.html>.",
             "Catalyst is free software: you are free to change and redistribute it.",
             "There is NO WARRANTY, to the extent permitted by law.",
             "",
             "Written by Amin Bandali <https://shemshak.org/~amin>."
         },
         description = "A framework for analysis/optimization of performance of Alloy models.",
         customSynopsis = "@|bold catalyst|@ [OPTION] COMMAND",
         mixinStandardHelpOptions = true,
         subcommands = { SSR.class, Mut.class, View.class })
public final class MainCLI implements Runnable {
    public static void main(String[] args) {
        System.exit(new CommandLine(new MainCLI()).execute(args));
    }

    public void run () {
        new CommandLine(new MainCLI()).usage(System.out);
    }
}
