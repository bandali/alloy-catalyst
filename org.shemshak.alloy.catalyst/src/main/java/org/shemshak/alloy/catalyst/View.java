/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.Gson;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;


@Command(name = "view", description = "Viewer for generated output files")
public final class View extends CatalystCommand implements Runnable {
    @Option(names = "--editor",
            description = "The editor to open the files in.  Falls back to $EDITOR")
    protected String editor = System.getenv("EDITOR");

    public View() {}

    public void run() {
        gson = new Gson();
        for (String corpus : corpora)
            analyzeCorpus(corpus);
    }

    protected CorpusResult analyzeCorpus(String corpus) {
        try {
            List<Path> files = Files.walk(Paths.get(outDir, corpus))
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());
            Stream<Path> fs = parallel ? files.parallelStream() : files.stream();
            AtomicInteger i = new AtomicInteger(1);
            fs.forEach(p -> analyzeFile(corpus, p, i.getAndIncrement(), files.size()));
        } catch (Exception e) {
            System.out.println("Failed while trying to iterate through the " + corpus + " corpus");
            System.out.println(Utils.toString(e));
        }
        return null;
    }

    protected FileResult analyzeFile(String corpus, Path p, int index, int total) {
        try (BufferedReader reader =
             new BufferedReader(new InputStreamReader(new FileInputStream(p.toString()),
                                                      StandardCharsets.UTF_8))) {
            FileResult fr = gson.fromJson(reader, FileResult.class);
            if (fr != null) {
                System.out.println("Opening:");
                System.out.println("  Result file: " + p.toString());
                System.out.println("  Original file: " + fr.file_name);
                Process p1 = new ProcessBuilder(editor, p.toString()).start();
                new ProcessBuilder(editor, fr.file_name).start().waitFor();
                if (p1.isAlive())
                    p1.waitFor();
            } else
                throw new Exception("Failed to parse JSON back into FileResult");
        } catch (Exception e) {
            System.out.println("Failed to read or parse " + p.toString());
            System.out.println(Utils.toString(e));
        }
        return null;
    }
}
