/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2018-2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import edu.mit.csail.sdg.alloy4.A4Reporter;
import edu.mit.csail.sdg.alloy4.ConstList;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.ErrorWarning;
import edu.mit.csail.sdg.ast.Sig;
import edu.mit.csail.sdg.parser.CompModule;
import edu.mit.csail.sdg.parser.CompUtil;
import edu.mit.csail.sdg.translator.A4Options;
import edu.mit.csail.sdg.translator.A4Options.SatSolver;
import edu.mit.csail.sdg.translator.TranslateAlloyToKodkod;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;


enum Solver {
    minisat(SatSolver.MiniSatJNI),
    minisatprover(SatSolver.MiniSatProverJNI),
    glucose(SatSolver.GlucoseJNI),
    sat4j(SatSolver.SAT4J);

    public final SatSolver solver;

    private Solver(SatSolver solver) {
        this.solver = solver;
    }
}

enum MutationKind {
    ORIGINAL,
    MUT_RANDOM
}

enum SolverResult {
    SAT, UNSAT
}


@Command(name = "mut", description = "Mutation analysis mode")
public final class Mut extends CatalystCommand implements Runnable {
    @Option(names = "--solvers",
                    split = ",",
                    paramLabel = "<solver>",
                    description = { "Comma-separated list of SAT solvers to be used",
                                    "Possible values: ${COMPLETION-CANDIDATES}"})
    protected Solver[] solvers = { Solver.minisat, Solver.minisatprover,
                                   Solver.glucose, Solver.sat4j };

    @Option(names = "--mutation-count",
            description = "Number of mutations to generate per Alloy file")
    protected int mutationCount = 3;

    final transient private String
        OUT_TEXT = "catalyst_mut.txt",
        OUT_JSON = "catalyst_mut.json",
        ERR_JSON = "catalyst_mut_errors.json";

    public Mut() {}

    public void run() {
        Path
            ot = Paths.get(outDir, OUT_TEXT),
            oj = Paths.get(outDir, OUT_JSON),
            ej = Paths.get(outDir, ERR_JSON);
        initialize(ot, oj, ej);

        // ugly workaround to load the solvers' native libraries
        // right off the bat and get it over with, to avoid polluting
        // stdout for the first file's first execution
        new kodkod.engine.satlab.InitSolvers(Arrays.stream(solvers)
                                             .map(s -> s.solver)
                                             .toArray(SatSolver[]::new));

        Instant start = Instant.now();
        for (String corpus : corpora)
            r.results.add(analyzeCorpus(corpus));
        Instant end = Instant.now();

        r.total_error_count = errorCount.get();
        r.total_elapsed_time = Duration.between(start, end).toMillis();

        pln(String.format("total number of analyzed files: %d", r.total_file_count));
        pln(String.format("total error count: %d", r.total_error_count));
        pln(String.format("total elapsed time: %dms", r.total_elapsed_time));

        writerWriteFlushClose(wjson, gson.toJson(r), oj);
        writerWriteFlushClose(ejson, gson.toJson(errors), ej);

        ptxt.flush();
        ptxt.close();
    }

    @Override
    protected final CorpusResult analyzeCorpus(String corpus) {
        CorpusResult cr = new CorpusResult();
        cr.corpus_name = corpus;
        cr.file_results = parallel ?
            Collections.synchronizedList(new ArrayList<FileResult>()) :
            new ArrayList<FileResult>();

        pln();

        Instant start = Instant.now();
        try {
            List<Path> files = Files.walk(Paths.get(modelsDir, corpus))
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());
            cr.file_count = files.size();
            pln("number of " + corpus + " models: " + cr.file_count);
            r.total_file_count += cr.file_count;
            AtomicInteger i = new AtomicInteger(1);
            Stream<Path> fs = parallel ? files.parallelStream() : files.stream();
            fs.filter(p -> p.getFileName().toString().endsWith(".als"))
                .filter(p -> !p.toString().matches(ignore))
                .forEach(p -> cr.file_results.add(analyzeFile(corpus, p, i.getAndIncrement(), cr.file_count)));
        } catch (Exception e) {
            String msg = "Failed while trying to iterate through and analyze " + corpus + " models.";
            cr.error = String.format("%s%n%s", msg, Utils.toString(e));
            p(cr.error);
            throw Utils.systemExit(1);
        }
        Instant end = Instant.now();

        cr.elapsed_time = Duration.between(start, end).toMillis();
        pln(String.format("time taken to analyze %s models: %dms",
                          corpus, cr.elapsed_time));

        pln();

        return cr;
    }

    @Override
    protected final FileResult analyzeFile(String corpus, Path p, int index, int total) {
        FileResult fr = new FileResult();
        fr.file_name = p.toString();
        fr.mutation_results = new ArrayList<MutationResult>();
        fr.errors = new ArrayList<CatalystError>();

        Path outFile = Paths.get(outDir, corpus, fr.file_name.replace("/", outSep));
        Writer w = setupWriter(outFile, true);

        String progress = String.format("[%d/%d]: ", index, total);

        A4Reporter rep = new A4Reporter() {
            @Override
            public void warning(ErrorWarning msg) {
                fr.errors.add(new CatalystError(msg.toString().trim(), false));
            }
        };

        pln(progress + "start: " + fr.file_name);

        CompModule world;
        try {
            world = CompUtil.parseEverything_fromFile(rep, null, fr.file_name);
        } catch (Err e) {
            String
                msg = "Error in parsing or type checking.  Stack trace from the exception:",
                err = Utils.toString(e);
            pln(progress + "error: " + fr.file_name);
            CatalystError error = new CatalystError(String.format("%s%n%s", msg, err), true);
            fr.errors.add(error);
            errors.put(p, error);
            errorCount.getAndIncrement();
            return fr;
        }

        if (moduleHasCommands(world)) {
            try {
                fr.mutation_results.add(mutateModule(world, MutationKind.ORIGINAL));
            } catch (Err e) {
                String
                    msg = "Error while analyzing original module.  Stack trace from the exception:",
                    err = Utils.toString(e);
                pln(progress + "error: " + fr.file_name);
                CatalystError error = new CatalystError(String.format("%s%n%s", msg, err), true);
                fr.errors.add(error);
                errors.put(p, error);
                errorCount.getAndIncrement();
                return fr;
            }

            for (int i = 1; i <= mutationCount; i++) {
                shuffleSigs(world);
                try {
                    fr.mutation_results.add(mutateModule(world, MutationKind.MUT_RANDOM));
                } catch (Err e) {
                    String
                        msg = "Error while analyzing mutation.  Stack trace from the exception:",
                        err = Utils.toString(e);
                    pln(progress + "error: " + fr.file_name);
                    CatalystError error = new CatalystError(String.format("%s%n%s", msg, err), true);
                    fr.errors.add(error);
                    errors.put(p, error);
                    errorCount.getAndIncrement();
                    return fr;
                }
            }
        }

        pln(progress + "done: " + fr.file_name);

        writerWriteFlushClose(w, gson.toJson(fr), outFile);

        return fr;
    }

    private static boolean moduleHasCommands(CompModule module) {
        return module.getAllCommands().size() > 0;
    }

    private class RunReporter extends A4Reporter {
        public Run run = new Run();

        @Override
        public void resultSAT(Object command, long solvingTime, Object solution) {
            RunResult res = new RunResult();
            res.result = SolverResult.SAT;
            res.solving_time = solvingTime;
            run.takes.add(res);
        }

        @Override
        public void resultUNSAT(Object command, long solvingTime, Object solution) {
            RunResult res = new RunResult();
            res.result = SolverResult.UNSAT;
            res.solving_time = solvingTime;
            run.takes.add(res);
        }
    }

    private MutationResult mutateModule(CompModule module, MutationKind kind) throws Err {
        MutationResult mr = new MutationResult();
        mr.kind = kind;
        mr.sigs = module.getAllSigsMap().toString();
        mr.runs = parallel ?
            Collections.synchronizedList(new ArrayList<Run>()) :
            new ArrayList<>();

        for (Solver solver : solvers) {
            ConstList<Sig> allReachableSigs = module.getAllReachableSigs();
            A4Options options = new A4Options();
            options.solver = solver.solver;
            Stream<edu.mit.csail.sdg.ast.Command> cs = parallel ?
                module.getAllCommands().parallelStream() : module.getAllCommands().stream();
            cs.forEach(c -> {
                    RunReporter rep = new RunReporter();
                    Run run = new Run();
                    run.solver_id = solver.solver.id();
                    run.solver_name = solver.solver.toString();
                    run.command = c.toString();
                    run.takes = Collections.synchronizedList(new ArrayList<RunResult>());
                    rep.run = run;
                    IntStream is = parallel ?
                        IntStream.range(0, 3).parallel() : IntStream.range(0, 3);
                    is.forEach(i -> {
                            TranslateAlloyToKodkod.execute_command(rep, allReachableSigs, c, options);
                        });
                    mr.runs.add(run);
                });
        }

        return mr;
    }

    private static void shuffleSigs(CompModule module) {
        List<Entry<String,Sig>> sigs = new ArrayList<>();
        for (Entry<String,Sig> e : module.getAllSigsMap().entrySet()) {
            sigs.add(e);
        }
        module.getAllSigsMap().clear();
        Collections.shuffle(sigs);
        for (Entry<String,Sig> e : sigs) {
            module.getAllSigsMap().put(e.getKey(), e.getValue());
        }
    }
}
