/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import java.util.List;


public class Result {
    public CatalystCommand options;
    public List<CorpusResult> results;
    public int total_file_count;
    public int total_error_count;
    public long total_elapsed_time;
    public String error;
}

class CorpusResult {
    public String corpus_name;
    public int file_count;
    public transient List<FileResult> file_results;
    public long elapsed_time;
    public SSRResult ssr_result;
    public String error;
}

class CatalystError {
    private String msg;
    private boolean fatal;

    public CatalystError() {}
    public CatalystError(String msg, boolean fatal) {
        this.msg = msg;
        this.fatal = fatal;
    }
}

class FileResult {
    public String file_name;
    public SSRResult ssr_result;
    public List<MutationResult> mutation_results;
    public List<CatalystError> errors;
}

class SSRResult {
    public long sig_count;
    public long ssr_candidate_count;
    public List<String> ssr_candidates;
}

class MutationResult {
    public MutationKind kind;
    public String sigs;
    public List<Run> runs;
}

class Run {
    public String solver_id;
    public String solver_name;
    public String command;
    public List<RunResult> takes;
}

class RunResult {
    public SolverResult result;
    public long solving_time;
}
