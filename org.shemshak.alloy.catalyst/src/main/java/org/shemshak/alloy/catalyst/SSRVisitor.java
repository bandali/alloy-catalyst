/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import java.util.LinkedHashMap;
import java.util.Map;

import edu.mit.csail.sdg.alloy4.Pos;
import edu.mit.csail.sdg.ast.Decl;
import edu.mit.csail.sdg.ast.Expr;
import edu.mit.csail.sdg.ast.ExprBadJoin;
import edu.mit.csail.sdg.ast.ExprBinary;
import edu.mit.csail.sdg.ast.ExprCall;
import edu.mit.csail.sdg.ast.ExprConstant;
import edu.mit.csail.sdg.ast.ExprCustom;
import edu.mit.csail.sdg.ast.ExprITE;
import edu.mit.csail.sdg.ast.ExprLet;
import edu.mit.csail.sdg.ast.ExprList;
import edu.mit.csail.sdg.ast.ExprQt;
import edu.mit.csail.sdg.ast.ExprUnary;
import edu.mit.csail.sdg.ast.ExprVar;
import edu.mit.csail.sdg.ast.Sig;
import edu.mit.csail.sdg.ast.Sig.Field;
import edu.mit.csail.sdg.ast.Sig.PrimSig;
import edu.mit.csail.sdg.ast.Type.ProductType;
import edu.mit.csail.sdg.ast.VisitReturn;
import edu.mit.csail.sdg.parser.CompModule;


/**
 * Structure Signature Removal (SSR) Visitor
 */

public class SSRVisitor extends VisitReturn<Object> {

    Map<PrimSig, Boolean> candidates = new LinkedHashMap<>();
    private CompModule world;

    public SSRVisitor(CompModule world) {
        this.world = world;
    }

    PrimSig getFirstPrimSig(Expr e) {
        return
            e.type()            // :: Type
            .iterator()         // :: Iterator<ProductType>
            .next()             // :: ProductType
            .get(0);            // :: PrimSig
    }

    boolean isFieldAccess(ExprBinary e) {
        ExprUnary
            l = (ExprUnary) e.left,
            r = (ExprUnary) e.right;
        return
            (l.sub instanceof Sig && r.sub instanceof Field
             && l.sub == ((Field) r.sub).sig)
            ||
            (l.sub instanceof ExprVar && r.sub instanceof Field
             && getFirstPrimSig(l) == getFirstPrimSig(r));
    }

    boolean isUsedInOpen(Sig s) {
        for (CompModule.Open o : world.getOpens())
            for (String arg : o.args)
                if (s == world.getAllSigsMap().get(arg))
                    return true;
        return false;
    }

    /** Visits an ExprBinary node. */
    @Override
    public Object visit(ExprBinary x) {
        // in a dot join, if it's not of form `sig.field' or `var.field' where
        // `var in sig', discard candidate
        if (x.op == ExprBinary.Op.JOIN) {
            if (x.left instanceof ExprUnary && x.right instanceof ExprUnary)
                if (isFieldAccess(x)) {
                    PrimSig s = getFirstPrimSig(x.left);
                    if (!(s.builtin || isUsedInOpen(s)))
                        candidates.putIfAbsent(s, true);
                }
        }
        else {
            if (x.left instanceof ExprUnary && x.right instanceof ExprUnary) {
                ExprUnary
                    l = (ExprUnary) x.left,
                    r = (ExprUnary) x.right;
                for (ProductType t : l.type())
                    for (int i = 1; i < t.arity(); i++)
                        if (!t.get(i).builtin)
                            candidates.put(t.get(i), false);
                for (ProductType t : r.type())
                    for (int i = 1; i < t.arity(); i++)
                        if (!t.get(i).builtin)
                            candidates.put(t.get(i), false);
            }
        }
        visitThis(x.left);
        visitThis(x.right);
        return null;
    }

    /** Visits an ExprList node. */
    @Override
    public Object visit(ExprList x) {
        for (Expr e : x.args)
            visitThis(e);
        return null;
    }

    /** Visits an ExprCall node. */
    @Override
    public Object visit(ExprCall x) {
        for (Expr e : x.args)
            visitThis(e);
        return null;
    }

    /** Visits an ExprConstant node. */
    @Override
    public Object visit(ExprConstant x) { return null; }

    /** Visits an ExprITE node. */
    @Override
    public Object visit(ExprITE x) {
        visitThis(x.cond);
        visitThis(x.left);
        visitThis(x.right);
        return null;
    }

    /** Visits an ExprLet node. */
    @Override
    public Object visit(ExprLet x) {
        visitThis(x.var);
        visitThis(x.expr);
        visitThis(x.sub);
        return null;
    }

    Expr getUnarySub(Expr e) {
        return ((ExprUnary)e).sub;
    }

    /** Visits an ExprQt node. */
    @Override
    public Object visit(ExprQt x) {
        // in a quantified expression, if the type of a variable has
        // more than one field, discard the corresponding sig
        for (Decl d : x.decls) {
            Expr s = d.expr.deNOP();
            if (s instanceof ExprUnary) {
                if (getUnarySub(s) instanceof ExprUnary) {
                    Expr subsub = getUnarySub(getUnarySub(s));
                    if (subsub instanceof PrimSig)
                        if (((PrimSig)subsub).getFieldDecls().size() > 1)
                            if (!((PrimSig)subsub).builtin)
                                candidates.put((PrimSig)subsub, false);
                }
            }
            else if (s instanceof PrimSig) {
                if (((PrimSig)s).getFieldDecls().size() > 1)
                    if (!((PrimSig)s).builtin)
                        candidates.put((PrimSig)s, false);
            }
            /**
             * else: mostly `ExprVar's with empty type which we don't
             * care about at all, or `ExprBinary's, which I think we
             * handle well enough in their own visit(ExprBinary) call.
             */
        }
        for (Decl d : x.decls)
            visitThis(d.expr);
        visitThis(x.sub);
        return null;
    }

    /** Visits a ExprBadJoin node */
    @Override
    public Object visit(ExprBadJoin x) {
        /**
         * Not sure how the type checker exactly works, but it seems
         * to flag some "false positive" fun/pred calls in (some?)
         * ExprQt sub-expressions with ExprBadJoin, resulting in a
         * call to VisitReturn's visit(ExprBadJoin) which in turn
         * throws an exception.  So, as a workaround, I'm overriding
         * it and returning null instead without throwing.
         *
         * To see an example, comment out this override, and use
         * SSRVisitor on abstractMemory.als or cacheMemory.als from
         * Chapter 6 of dnj's Software Abstractions book.
         */
        return null;
    }

    /** Visits a ExprCustom node */
    @Override
    public Object visit(ExprCustom x) {
        /**
         * Similar to ExprBadJoin case above, but for Macro.  Happens
         * in models/builtin/puzzle/coloring/color-australia.als, with
         * a 'Incomplete call on the macro "adjacent"' message.
         */
        return null;
    }

    /** Visits an ExprUnary node. */
    @Override
    public Object visit(ExprUnary x) {
        visitThis(x.sub);
        return null;
    }

    /** Visits an ExprVar node. */
    @Override
    public Object visit(ExprVar x) { return null; }

    /** Visits a Sig node. */
    @Override
    public Object visit(Sig x) {
        if (x instanceof PrimSig) {
            PrimSig px = (PrimSig)x;
            /*
            if (!px.builtin && !candidates.containsKey(px))
                candidates.put(px, true);
            */
            if (px.isMeta != null)
                candidates.put(px, false);
            if (px.isAbstract != null)
                candidates.put(px, false);
            if (px.parent != null && px.parent.isEnum != null) {
                candidates.put(px, false);
                candidates.put(px.parent, false);
            }
        }
        else if (x instanceof Sig.SubsetSig) {
            for (Sig p : ((Sig.SubsetSig)x).parents)
                if (p instanceof PrimSig)
                    candidates.put((PrimSig)p, false);
        }
        return null;
    }

    /** Visits a Field node. */
    @Override
    public Object visit(Field x) {
        // System.out.println("field: " + x + " decl: " + x.decl().expr);
        // System.out.println("field type: " + x.type());

        // discard sigs (including x.sig) used in this field's type
        // after first column
        for (ProductType t : x.type())
            for (int i = 1; i < t.arity(); i++)
                if (!t.get(i).builtin)
                    candidates.put(t.get(i), false);

        if (x.isMeta != null)
            if (x.sig instanceof PrimSig)
                candidates.put((PrimSig)x.sig, false);

        // TODO: if type of field refers to another field, in this sig
        // or elsewhere, if there are no multiplicity constraints then
        // we're fine (we can replace the type with that sig's type).
        // otherwise, do we have to discard this or that sig?

        return null;
    }

    static String posLineCol(Pos p) {
        return "(" + p.y + ", " + p.x + ")"
            + " -- "
            + "(" + p.y2 + ", " + p.x2 + ")";
    }

    static void printExpr(Expr e) {
        System.out.print(posLineCol(e.pos()) + " -> ");
        System.out.print(e + " ");
        System.out.print(": " + e.type() + " ");
        System.out.print(":: " + e.getClass());
        System.out.println();
    }
}
