/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;


@Command(showDefaultValues = true)
public abstract class CatalystCommand {
    protected transient Result r;
    protected transient Gson gson;
    protected transient AtomicInteger errorCount;
    protected transient Writer wtxt, wjson, ejson;
    protected transient PrintWriter ptxt;

    @Option(names = "--models-dir",
                    required = true,
                    description = "Directory containing the corpora of Alloy models")
    protected String modelsDir;

    @Option(names = "--corpora",
                    paramLabel = "<corpus>",
                    required = true,
                    split = ",",
                    description = "Comma-separated list of corpora directories inside the" +
                                  " models directory to be analyzed")
    protected String[] corpora;

    @Option(names = "--out-dir",
                    required = true,
                    description = "Output directory for analysis results and other Catalyst output")
    protected String outDir;

    @Option(names = "--separator",
                    description = "Separator used instead of forward slash in output file" +
                                  " names; e.g. with \"$_\", /a/b/c becomes $_a$_b$_c")
    protected String outSep = "$_";

    @Option(names = "--ignore",
                    paramLabel = "<regex>",
                    description = "file paths matching this regex will be skipped")
    protected String ignore = "(.*)ga-optodeclspecs(.*)|nasa_fgs_mode_logic\\.als"; // takes way too long

    @Option(names = "--parallel", description = "Parallel mode (use parallel streams)")
    protected boolean parallel;

    @Option(names = "--pretty", description = "Pretty-print JSON output to make it more human-readable")
    protected boolean pretty;

    final transient protected Map errors = parallel ?
        Collections.synchronizedMap(new LinkedHashMap<Path, CatalystError>()) :
        new LinkedHashMap<Path, CatalystError>();

    protected void initialize(Path outputTxtFile, Path outputJsonFile, Path errorsJsonFile) {
        r = new Result();
        r.options = this;
        r.results = new ArrayList<CorpusResult>();
        gson = pretty ?
            new GsonBuilder().setPrettyPrinting().create() :
            new Gson();
        errorCount = new AtomicInteger();
        wtxt = setupWriter(outputTxtFile, true);
        wjson = setupWriter(outputJsonFile, true);
        ejson = setupWriter(errorsJsonFile, true);
        ptxt = new PrintWriter(wtxt);
    }

    abstract protected CorpusResult analyzeCorpus(String corpus);
    abstract protected FileResult analyzeFile(String corpus, Path p, int index, int total);

    protected BufferedWriter setupWriter(Path p, boolean createParents) {
        if (createParents)
            p.getParent().toFile().mkdirs();

        BufferedWriter w;
        try {
            w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(p.toString()),
                                                          StandardCharsets.UTF_8));
        }
        catch (FileNotFoundException e) {
            String msg = "Failed to set up file output stream for " +
                p.toString();
            r.error = String.format("%s%n%s", msg, Utils.toString(e));
            System.out.print(r.error);
            throw Utils.systemExit(1);
        }
        return w;
    }

    protected void writerWrite(Writer w, String s, Path f) {
        try {
            w.write(s);
        }
        catch (IOException e) {
            pf("I/O error occured while trying to write to %s:%n", f.toString());
            pln(Utils.toString(e));
        }
    }

    protected void writerFlush(Writer w, Path f) {
        try {
            w.flush();
        }
        catch (IOException e) {
            pf("I/O error occured while trying to flush %s writer:%n", f.toString());
            pln(Utils.toString(e));
        }
    }

    protected void writerClose(Writer w, Path f) {
        try {
            w.close();
        }
        catch (IOException e) {
            pf("I/O error occured while trying to close %s writer:%n", f.toString());
            pln(Utils.toString(e));
        }
    }

    protected void writerWriteFlushClose(Writer w, String s, Path f) {
        writerWrite(w, s, f);
        writerFlush(w, f);
        writerClose(w, f);
    }

    protected void p(String s) {
        System.out.print(s);
        ptxt.print(s);
    }

    protected void pln(String s) {
        p(s + System.lineSeparator());
    }

    protected void pln() {
        pln("");
    }

    protected void pf(String format, Object... args) {
        p(String.format(format, args));
    }
}
