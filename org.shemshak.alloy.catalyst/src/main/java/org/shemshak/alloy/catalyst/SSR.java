/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import java.io.File;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.mit.csail.sdg.alloy4.A4Reporter;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.ErrorWarning;
import edu.mit.csail.sdg.parser.CompModule;
import edu.mit.csail.sdg.parser.CompUtil;
import org.apache.commons.math3.fraction.BigFraction;
import picocli.CommandLine.Command;


@Command(name = "ssr", description = "Structure Signature Removal mode")
public final class SSR extends CatalystCommand implements Runnable {
    final transient private String
        OUT_TEXT = "catalyst_ssr.txt",
        OUT_JSON = "catalyst_ssr.json",
        ERR_JSON = "catalyst_ssr_errors.json";

    public SSR() {}

    public void run() {
        Path
            ot = Paths.get(outDir, OUT_TEXT),
            oj = Paths.get(outDir, OUT_JSON),
            ej = Paths.get(outDir, ERR_JSON);
        initialize(ot, oj, ej);

        Instant start = Instant.now();
        for (String corpus : corpora)
            r.results.add(analyzeCorpus(corpus));
        Instant end = Instant.now();

        r.total_error_count = errorCount.get();
        r.total_elapsed_time = Duration.between(start, end).toMillis();

        pln(String.format("total number of analyzed files: %d", r.total_file_count));
        pln(String.format("total error count: %d", r.total_error_count));
        pln(String.format("total elapsed time: %dms", r.total_elapsed_time));

        writerWriteFlushClose(wjson, gson.toJson(r), oj);
        writerWriteFlushClose(ejson, gson.toJson(errors), ej);

        ptxt.flush();
        ptxt.close();
    }

    @Override
    protected final CorpusResult analyzeCorpus(String corpus) {
        CorpusResult cr = new CorpusResult();
        cr.corpus_name = corpus;

        pln();

        Instant start = Instant.now();
        try {
            List<Path> files = Files.walk(Paths.get(modelsDir, corpus))
                .filter(Files::isRegularFile)
                .filter(p -> p.getFileName().toString().endsWith(".als"))
                .filter(p -> !p.toString().matches(ignore))
                .collect(Collectors.toList());
            cr.file_count = files.size();
            pln("number of " + corpus + " models: " + cr.file_count);
            r.total_file_count += cr.file_count;
            AtomicInteger i = new AtomicInteger(1);
            Stream<Path> fs = parallel ? files.parallelStream() : files.stream();
            cr.file_results =
                fs.map(p -> analyzeFile(corpus, p, i.getAndIncrement(), cr.file_count))
                .collect(Collectors.toList());
        } catch (Exception e) {
            String msg = "Failed while trying to iterate through and analyze " + corpus + " models.";
            cr.error = String.format("%s%n%s", msg, Utils.toString(e));
            p(cr.error);
            throw Utils.systemExit(1);
        }
        Instant end = Instant.now();

        cr.elapsed_time = Duration.between(start, end).toMillis();
        pln(String.format("time taken to analyze %s models: %dms",
                          corpus, cr.elapsed_time));

        p("Calculating corpus stats...");
        long ssrs = 0, ssrc = 0, count = 0;
        BigFraction avgOfRatios = new BigFraction(0, 1);
        for (FileResult fr : cr.file_results)
            if (fr.ssr_result != null) {   // happens when there are
                                           // errors in analyzeFile
                                           // and it exits early
                long
                    num = fr.ssr_result.ssr_candidate_count,
                    den = fr.ssr_result.sig_count;
                ssrs += den;
                ssrc += num;
                if (den != 0) {
                    avgOfRatios = avgOfRatios.add(new BigFraction(num, den));
                    count++;
                }
            }
        if (count != 0)
            avgOfRatios = avgOfRatios.divide(count);
        pln(" done");

        pln(String.format("SSR candidates: %d/%d (%.2f%%)",
                          ssrc, ssrs, 100. * ssrc / ssrs));
        if (count != 0)
            pln(String.format("SSR avg of ratios: %s/%s (%.2f%%)",
                              avgOfRatios.getNumerator(),
                              avgOfRatios.getDenominator(),
                              avgOfRatios.percentageValue()));
        cr.ssr_result = new SSRResult();
        cr.ssr_result.sig_count = ssrs;
        cr.ssr_result.ssr_candidate_count = ssrc;

        pln();

        return cr;
    }

    @Override
    protected final FileResult analyzeFile(String corpus, Path p, int index, int total) {
        FileResult fr = new FileResult();
        fr.file_name = p.toString();
        fr.errors = new ArrayList<CatalystError>();

        String progress = String.format("[%d/%d]: ", index, total);
        String finalStatus = "done";

        Path prefix = Paths.get(outDir, corpus);
        Path fName;
        try {
            int diff = outSep.length() - 1 /* length of separatorChar */;
            int wiggleRoom =
                prefix.toString().length() + 1 /* one joining separatorChar */ +
                Utils.countMatches(fr.file_name, File.separatorChar) * diff;
            fName = Utils.shortenPathIfNeeded(Paths.get(fr.file_name), 255, wiggleRoom);
        }
        catch (PathTooLongException e) {
            String
                msg = "Error in creating output json file:",
                err = Utils.toString(e);
            pln(progress + "error: " + fr.file_name);
            CatalystError error = new CatalystError(String.format("%s%n%s", msg, err), true);
            fr.errors.add(error);
            errors.put(p, error);
            errorCount.getAndIncrement();
            return fr;
        }
        Path outFile = Paths.get(prefix.toString(),
                                 fName.toString().replace(File.separator, outSep));
        Writer w = setupWriter(outFile, true);

        A4Reporter rep = new A4Reporter() {
            @Override
            public void warning(ErrorWarning msg) {
                fr.errors.add(new CatalystError(msg.toString().trim(), false));
            }
        };

        pln(progress + "start: " + fr.file_name);

        CompModule world;
        try {
            world = CompUtil.parseEverything_fromFile(rep, null, fr.file_name);
            fr.ssr_result = detectRemovableSigs(world);
        } catch (Err e) {
            String
                msg = "Error in parsing or type checking.  Stack trace from the exception:",
                err = Utils.toString(e);
            CatalystError error = new CatalystError(String.format("%s%n%s", msg, err), true);
            fr.errors.add(error);
            errors.put(p, error);
            errorCount.getAndIncrement();
            finalStatus = "error";
        }

        pln(progress + finalStatus + ": " + fr.file_name);

        writerWriteFlushClose(w, gson.toJson(fr), outFile);

        return fr;
    }

    private SSRResult detectRemovableSigs(CompModule world) {
        SSRVisitor v = new SSRVisitor(world);
        world.visitExpressions(v);
        SSRResult r = new SSRResult();
        r.sig_count = v.candidates.size();
        r.ssr_candidates = new ArrayList<String>();
        v.candidates.entrySet()
            .stream()
            .forEach(entry -> {
                    if (entry.getValue().equals(true))
                        r.ssr_candidates.add(entry.getKey().toString());
                });
        r.ssr_candidate_count = r.ssr_candidates.size();
        return r;
    }
}
