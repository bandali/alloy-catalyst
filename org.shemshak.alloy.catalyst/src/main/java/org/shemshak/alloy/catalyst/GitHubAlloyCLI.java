/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2018-2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.PagedSearchIterable;

public class GitHubAlloyCLI {

    public static void main(String[] args) {
        String query = "language:alloy NOT ableton NOT midi NOT music NOT mIRC -repo:AlloyTools/models";
        boolean showDescriptions = false;
        boolean useSSHUrl = false;    // whether to use the SSH protocol for cloning, or HTTPS
        boolean prependSHA256 = true; // whether to prepend SHA-256 of url to folder name (to obtain unique names)
        boolean makeCloneList = true; // format output as a convenient list of "git clone" commands
        makeCloneList = makeCloneList && !showDescriptions; // only make clone list if showDescriptions is false

        GitHub gh;
        try {
            gh = GitHub.connect();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (gh == null) {
            System.out.println("connection to GitHub failed");
            return;
        }
        System.out.println("connected to GitHub");

        PagedSearchIterable<GHRepository> repos = gh.searchRepositories().q(query).list();

        System.out.println("search query: " + query);
        System.out.println("# of results: " + repos.getTotalCount());
        System.out.println();

        for (GHRepository repo : repos) {
            String str = makeCloneList ? "git clone" : "";

            String url = useSSHUrl ? repo.getSshUrl() : repo.getHttpTransportUrl();
            str += " " + url;

            if (showDescriptions)
                str += " " + repo.getDescription();
            else if (makeCloneList) {
                str += " ";
                if (prependSHA256)
                    str += Utils.sha256_32(url) + "-";
                str += repo.getName();
            }

            System.out.println(str);
        }
    }

}
