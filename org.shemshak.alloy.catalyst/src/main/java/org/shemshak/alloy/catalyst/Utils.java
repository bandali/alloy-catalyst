/*
 * Catalyst -- A framework for performance analysis/optimization of Alloy models
 * Copyright (C) 2018-2019 Amin Bandali
 *
 * This file is part of Catalyst.
 *
 * Catalyst is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Catalyst is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Catalyst.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.shemshak.alloy.catalyst;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_256;

import org.apache.commons.codec.digest.DigestUtils;

public class Utils {

    public static String sha256(String s) {
        return new org.apache.commons.codec.binary.Base32().encodeAsString(new DigestUtils(SHA_256).digest(s));
    }

    public static String sha256_n(String s, int n) {
        return sha256(s).substring(0, n).toLowerCase();
    }

    public static String sha256_32(String s) {
        return sha256_n(s, 32);
    }

    public static RuntimeException systemExit(int status) {
        System.exit(status);
        return new RuntimeException("This should never have happened.");
    }

    public static String toString(Throwable ex) {
        StringBuilder sb = new StringBuilder();
        while(ex != null) {
            sb.append(ex.getClass())
                .append(": ")
                .append(ex.getMessage())
                .append('\n');
            StackTraceElement[] trace = ex.getStackTrace();
            if (trace != null)
                for (int i = 0; i < trace.length; i++)
                    sb.append(trace[i]).append('\n');
            ex = ex.getCause();
            if (ex != null)
                sb.append("caused by...\n");
        }
        return sb.toString().trim();
    }

    public static int countMatches(String s, char c) {
        if (s.isEmpty())
            return 0;
        int count = 0;
        for (int i = 0; i < s.length(); i++)
            if (c == s.charAt(i))
                count++;
        return count;
    }

    public static Path shortenPathIfNeeded(Path p, int max, int wiggleRoom)
        throws PathTooLongException {
        if (p.toString().length() > max - wiggleRoom) {
            Path parent = p;
            for (int i = 0; i < countMatches(p.toString(), File.separatorChar); i++) {
                parent = parent.getParent();
                String parentName = parent.getFileName().toString();
                if (parentName.length() > 16)
                    p = Paths.get(p.toString()
                                  .replace(parentName, sha256_n(parentName, 16)));
                if (p.toString().length() <= max - wiggleRoom)
                    return p;
            }
            throw new PathTooLongException("Path too long and/or too deeply nested");
        }
        else
            return p;
    }
}

class PathTooLongException extends Exception {
    public PathTooLongException(String msg) {
        super(msg);
    }

    public PathTooLongException(String msg, Throwable err) {
        super(msg, err);
    }
}
