package kodkod.engine.satlab;

import java.util.Arrays;
import java.util.List;

import edu.mit.csail.sdg.translator.A4Options.SatSolver;

public class InitSolvers {
    public InitSolvers(SatSolver[] solvers) {
        List<SatSolver> s = Arrays.asList(solvers);
        if (s.contains(SatSolver.MiniSatJNI))
            new kodkod.engine.satlab.MiniSat();
        if (s.contains(SatSolver.MiniSatProverJNI))
            new kodkod.engine.satlab.MiniSatProver();
        if (s.contains(SatSolver.GlucoseJNI))
            new kodkod.engine.satlab.Glucose();
    }
}
